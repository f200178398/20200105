﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class AudioManager : MonoBehaviour
{

    [Header("各個BGM的物件")]
    public GameObject go_AudioManager;//這個物體本身
    public AudioSource as_NormalBGM;
    public AudioSource as_BossBGM;
    public AudioSource as_KyleBattleBGM;

    //找Timeline的腳本裡的Playable
    [Header("各個要切換BGM的Trigger")]
    public TimelineManager tm_KyleBattleTimelineTrigger;
    public TimelineManager tm_BossTimelineTrigger;

    //找各個相機的位置
    [Header("相機的位置")]
     public GameObject go_mainCamera;
    [HideInInspector] public GameObject go_KyleBattleTimelineCamera;
    [HideInInspector] public GameObject go_BossTimelineCamera;

    //[Header("各個相機的AudioListener")]
    //public AudioListener al_mainCameraListener;
    //public AudioListener al_KyleBattleListener;
    //public AudioListener al_BossTimelineListener;

    //[Header("各個AudioClip(那個音樂)的檔案")]
    [HideInInspector]public AudioClip ac_NormalBGM;
    [HideInInspector] public AudioClip ac_KyleBattleBGM;
    [HideInInspector] public AudioClip ac_BossBGM;

    [Header("監測用，控制音樂開關的布林值")]
    public bool b_NormalBGM;
    public bool b_KyleBattleBGM;
    public bool b_BossBGM;


    public void Awake()
    {
        go_AudioManager = this.gameObject;
        as_NormalBGM = GameObject.FindGameObjectWithTag("NormalBGM").GetComponent<AudioSource>();
        as_NormalBGM.PlayDelayed(3.0f);
        as_KyleBattleBGM = GameObject.FindGameObjectWithTag("KyleBattleBGM").GetComponent<AudioSource>();
        as_BossBGM = GameObject.FindGameObjectWithTag("BossBattleBGM").GetComponent<AudioSource>();
    }

    public void Start()
    {        
        
        DontDestroyOnLoad(go_AudioManager);
    }
    public void Update()
    {
        PlayBGM();
    }

    public void PlayBGM()
    {
        Debug.Log("normalBGM=" + as_NormalBGM.isPlaying);
        Debug.Log("battleKyleBGM=" + as_KyleBattleBGM.isPlaying);
        Debug.Log("bossBGM=" + as_BossBGM.isPlaying);



        if ( !tm_KyleBattleTimelineTrigger.gameObject || !tm_BossTimelineTrigger.gameObject)
        {
            Debug.Log("沒有設定KyleBattle和BossBattle的Trigger");
        }

        if ( tm_KyleBattleTimelineTrigger.timelineTrigger && tm_BossTimelineTrigger.b_hasBeenTiggered == false )
        {
            Debug.Log("koko");
            as_NormalBGM.Stop();
            as_BossBGM.Stop();
            b_KyleBattleBGM = true;
        }
        if (tm_BossTimelineTrigger.timelineTrigger && tm_BossTimelineTrigger.b_hasBeenTiggered )
        {
            Debug.Log("koko++");
            as_NormalBGM.Stop();
            as_KyleBattleBGM.Stop();
            b_BossBGM = true;
        }


        PlayKyleBattleBGM();
        PlayBossBGM();

        //if (tm_KyleBattleTimelineTrigger.b_hasBeenTiggered)
        //{
        //    as_NormalBGM.Stop();
        //    as_BossBGM.Stop();

        //if (tm_KyleBattleTimelineTrigger.timelineAnimation.state == PlayState.Playing)
        //{
        //    Debug.Log("here0");
        //    as_NormalBGM.Stop();
        //    //as_KyleBattleBGM.clip = ac_KyleBattleBGM;
        //    //as_KyleBattleBGM.PlayDelayed(0.5f);            
        //    as_KyleBattleBGM.Pause();
        //    as_BossBGM.Pause();
        //}
        //else if (tm_KyleBattleTimelineTrigger.timelineAnimation.state == PlayState.Paused)
        //{
        //    if (as_KyleBattleBGM.isPlaying) { return; }
        //    Debug.Log("here1");
        //    as_KyleBattleBGM.Play();               
        //}


        //if (tm_KyleBattleTimelineTrigger.timelineAnimation.state==PlayState.Playing)
        //{
        //    b_KyleBattleBGM = true;
        //    PlayKyleBattleBGM();
        //}
        //}       
        //else if (tm_BossTimelineTrigger.b_hasBeenTiggered)
        //{
        //    as_NormalBGM.Stop();
        //    as_KyleBattleBGM.Stop();

        //if (tm_BossTimelineTrigger.timelineAnimation.state == PlayState.Playing)
        //{
        //    Debug.Log("here2");
        //    as_BossBGM.Pause();
        //    as_NormalBGM.Stop();
        //    as_KyleBattleBGM.Stop();
        //}
        //else if (tm_BossTimelineTrigger.timelineAnimation.state == PlayState.Paused)
        //{
        //    if (as_BossBGM.isPlaying) { return; }
        //    Debug.Log("here3");
        //    as_BossBGM.clip = ac_BossBGM;
        //    as_BossBGM.time = 9.25f;
        //    as_BossBGM.Play();

        //}
        //}







    }

    public IEnumerator _PlayKyleBattle()
    {
        //yield return new WaitForSeconds(9.1f);
        yield return new WaitForSeconds(0.1f);
        //yield return null;
        as_KyleBattleBGM.Play();
        
    }
    public IEnumerator _PlayBoss()
    {
        //yield return new WaitForSeconds(9.25f);
        yield return new WaitForSeconds(0.1f);
        //yield return null;
        as_BossBGM.clip = ac_BossBGM;
        as_BossBGM.time = 9.25f;
        as_BossBGM.Play();
    }

    public void PlayNormalBGM()
    {
        if(!b_NormalBGM) { return; }
        as_NormalBGM.Play();
        b_NormalBGM = false;
    }
    public void PlayKyleBattleBGM()
    {
        if (!b_KyleBattleBGM) { return; }
        as_KyleBattleBGM.Play();
        b_KyleBattleBGM = false;
    }
    public void PlayBossBGM()
    {
        if (!b_BossBGM) { return; }
        as_BossBGM.Play();
        b_BossBGM = false;
    }
}
