﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class DefenseTower : MonoBehaviour
{
    public GameObject controller;
    public AI_data data;
    public float turretSpeed = 2f;

    void Start()
    {
        controller = GameObject.Find("Controller");
        data = gameObject.GetComponent<AI_data>();
    }

    // Update is called once per frame
    void Update()
    {
        TurretRotation();
    }
    void TurretRotation()
    {
        if (data.currentHp > 0)
        {
            //controller = GameObject.Find("Controller");
            //transform.position += ()

            var rotation = Quaternion.LookRotation(controller.transform.position - transform.position, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turretSpeed);
            //anim.SetBool("chase", true);
        }
        else
        {
            //transform.Translate(0, 0, 0);
            //anim.SetBool("dead", true);
            this.enabled = false;
        }
    }
}
