﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    public Camera mainCamera;
    public Camera timelineCamera;
    

    public PlayableDirector timelineAnimation;
    public bool timelineTrigger;

    public Collider thisTriggerCollider;
    public bool b_hasBeenTiggered;//給外面的人呼叫的
    public bool b_timelineAnimationHasPlayedOver;//給外面的人偵測動畫已經播完的



    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("魔王TimelineTigger和魔王的距離\t"+Vector3.Distance(this.transform.position, GameObject.Find("Boss").transform.position));
        thisTriggerCollider = GetComponent<Collider>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timelineAnimation != null)
        {
            //Debug.Log(timelineAnimation.state + timelineAnimation.name);//現在這個Timeline的狀態
        }
        //Debug.Log("timelineTrigger\t"+timelineTrigger + "\t" + name);
        //Debug.Log(b_hasBeenTiggered + "\t" + name);


        EveryTimelineNoTrigger();

        //測試用的按某個鍵
        if (Input.GetKeyDown(KeyCode.O))
        {
            //timelineAnimation.Stop();
            
            mainCamera.enabled = false;
            TimelineAnimationStart();
        }

        TimelineAnimationStart();
        
    }

    public void EveryTimelineNoTrigger()
    {
        mainCamera.enabled = true;
        if (timelineCamera!=null)
        {
            if (timelineAnimation.state == PlayState.Paused)
            {
                timelineCamera.enabled = false;
            }
            else if(timelineAnimation.state == PlayState.Playing)
            {
                timelineAnimation.enabled = true;
            }
        }
    }

    public void TimelineAnimationStart()
    {
        if (!timelineTrigger) { return; }
        if (timelineCamera != null)
        {
            timelineCamera.enabled = true;//切換攝影機
        }
        if(timelineAnimation != null)
        {
            timelineAnimation.Play();
        }
        
        
        
        timelineTrigger = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Controller")
        {
            timelineTrigger = true;
            b_hasBeenTiggered = true;
            thisTriggerCollider.enabled = false;
        }
        
    }
    
    //public void OnTriggerExit(Collider other)
    //{        
    //    timelineTrigger = false;
    //    thisTriggerCollider.enabled = false;
    //}

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position , new Vector3(10f, 5f, 3f));
    }

    
}
